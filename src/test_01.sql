select '****** TEST_01 STARTS HERE ****** >>';

\ir newCustomer.sql;


select newCustomer (1, 'A', 'p1', 0);
select newCustomer (2, 'B', 'p2', -2000);
select newCustomer (3, 'C', 'p3', -3000);
select newCustomer (4, 'D', 'p4', -4000);
select newCustomer (5, 'E', 'p5', -5000);
select newCustomer (6, 'F', 'p6', -6000);

select * from customers ORDER BY accountnum;

-- 
\ir closeCustomer.sql;

select closeCustomer(7);
select closeCustomer(2);

select * from customers ORDER BY accountnum;
select * from accountbalance ORDER BY accountnum;

select newCustomer (2, 'B - UPDATED', 'p2 - UPDATED', -7000);

select * from customers ORDER BY accountnum;
select * from accountbalance ORDER BY accountnum;

select closeCustomer(5);

select * from customers ORDER BY accountnum;
select * from accountbalance ORDER BY accountnum;

--
\ir doAction.sql;

select doAction(1, 'receive', current_date, 1001);
select doAction(2, 'receive', current_date, 2001);
select doAction(3, 'receive', current_date, 3001);
select doAction(4, 'receive', current_date, 4001);

select * from accountbalance ORDER BY accountnum;

select doAction(4, 'payment', current_date, -5000);
select doAction(3, 'payment', current_date, -2000);
select doAction(4, 'payment', current_date, -2000);
select doAction(1, 'payment', current_date, -2000);

select * from accountbalance ORDER BY accountnum;

--
\ir newSaving.sql;

select newSaving(2, 1000, date '12-12-2012', 2, 0.4);
select newSaving(2, 10, date '12-11-2011', 2, 0.6);
select newSaving(3, 100, date '12-10-2010', 2, 0.8);

select * from accountbalance ORDER BY accountnum;
select * from savings ORDER BY savingnum;

--
select * from top10customers;
