CREATE OR REPLACE FUNCTION doAction (
	_CustomerID Integer,
	_ActionName Varchar,
	_ActionDate Date,
	_Amount Real
	)
RETURNS Integer AS

$$

DECLARE

	id Integer;
	customer record;
	account record;

BEGIN

SELECT * INTO customer
FROM customers
WHERE customerId = _CustomerID;

IF NOT FOUND THEN RETURN -1;
END IF;

IF customer.accountstatus = 'close' THEN RETURN -1;
END IF;

IF _ActionName = 'receive'
	OR _ActionName = 'payment'
	OR _ActionName = 'receive'
	OR _ActionName = 'saving'
	OR _ActionName = 'close'
THEN
	SELECT * INTO account
	FROM accountBalance
	WHERE accountnum = customer.accountnum;

	IF NOT FOUND THEN
		RETURN -1;
	END IF;

	UPDATE accountBalance SET balance = balance + _Amount
	WHERE accountnum = account.accountnum;

	INSERT INTO actions (
		actionnum,
		accountnum,
		actionname,
		actiondate,
		amount
	)
	VALUES (
		DEFAULT,
		account.accountnum,
		_ActionName,
		_ActionDate,
		_Amount
	) RETURNING actionnum INTO id;

	RETURN id;

END IF;

RETURN -1;

END;

$$
LANGUAGE 'plpgsql'