
import java.sql.*;
import java.util.Date;

public class CheckMySavings {

	Connection con;
	Statement stmt;
	ResultSet rs;
	
	public static void main(String[] args) throws Exception {
		
		CheckMySavings check = new CheckMySavings();
		check.init("eyalc");
		System.out.printf("%f\n", check.checkMySavings(1, new Date()));
		System.out.printf("%f\n", check.checkMySavings(2, new Date()));
		System.out.printf("%f\n", check.checkMySavings(3, new Date()));
		System.out.printf("%f", check.checkMySavings(5, new Date()));
		check.close();
	}
	
	private static double calcSaving (double deposit, int numOfYears, double interest) {
		if (numOfYears == 1) {
			return deposit * (1 + interest);
		} else {
			return (deposit + calcSaving(deposit, numOfYears-1, interest)) * (1 + interest);
		}
	}
	
	public double checkMySavings (int AccountNum, Date openDate) {
		double result = 0.0;
		try {
			String query = "select * from customers where accountnum = " + AccountNum;
			this.rs = this.stmt.executeQuery(query);
			
			if (!rs.isBeforeFirst() ) {
				 close();
				 System.exit(-1);
			}
			
			query = "select * from savings where accountnum = " + AccountNum;
			this.rs = this.stmt.executeQuery(query);
			
			while(this.rs.next()) {
				Date depositDate = this.rs.getDate("depositdate");
				int numOfYears = this.rs.getInt("numofyears");
				double deposit = this.rs.getInt("deposit");
				double interest = this.rs.getDouble("interest");
				
				Date endDate = (Date) depositDate.clone();
			
				if(depositDate.after(openDate)) {

				} else {
					endDate.setYear(endDate.getYear() + numOfYears);
					if (endDate.after(openDate)) {
						int correction = 0;
						Date tempDate = (Date) depositDate.clone();
						tempDate.setYear(openDate.getYear());
						if(tempDate.before(openDate)) {
							correction = 1;
						}
						int numOfDeposits = openDate.getYear() - depositDate.getYear() + correction;
						result += numOfDeposits * deposit;
					} else {
						result += calcSaving(deposit, numOfYears, interest);
					}
				}
			}
		} catch (SQLException e) {
			while (e != null) {
				System.out.println(e.getSQLState());
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				e = e.getNextException();
				close();
				System.exit(-1);
			}
		} catch (Exception Exception) {
			System.out.println("Unknown Error.");
			close();
			System.exit(-1);
		}
		return result;
	}
	
	public void init (String username) {
		try {
			Class.forName("org.postgresql.Driver");
			this.con = DriverManager.getConnection("jdbc:postgresql://dbcourse/public?user=" + username);
			//this.con = DriverManager.getConnection("jdbc:postgresql://localhost/?user=" + username + "&password=admin");
			this.stmt = con.createStatement();
		}
		catch (SQLException e) {
			while (e != null) {
				System.out.println(e.getSQLState());
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				e = e.getNextException();
				System.exit(-1);
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			close();
			System.exit(-1);
		}
	}

	public void close () {
		try {
			this.stmt.close(); this.rs.close(); this.con.close();
		} catch (SQLException e) {
			while (e != null) {
				System.out.println(e.getSQLState());
				System.out.println(e.getMessage());
				System.out.println(e.getErrorCode());
				e = e.getNextException();
				System.exit(-1);
			}
		} catch (NullPointerException e) {
			System.out.println("No connection to close.");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("Unknown error.");
			System.exit(-1);
		}
	}
}
