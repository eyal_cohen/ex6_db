CREATE OR REPLACE FUNCTION newSaving (
	_CustomerID Integer,
	_Deposit Integer,
	_DepositDate Date,
	_NumOfYears Integer,
	_Interest Real
	)
RETURNS Integer AS

$$

DECLARE

	id Integer;
	customer record;

BEGIN

SELECT * INTO customer
FROM customers
WHERE customerid = _CustomerID;

IF NOT FOUND THEN
	RETURN -1;
ELSIF customer.accountstatus = 'close' THEN
	RETURN -1;
ELSE

	perform doAction(_CustomerID, 'saving', CURRENT_DATE ,(-1)*_Deposit);

	INSERT INTO savings (
		savingnum,
		accountnum,
		deposit,
		depositdate,
		numofyears,
		interest
	)
	VALUES (
		DEFAULT,
		customer.accountnum,
		_Deposit,
		_DepositDate,
		_NumOfYears,
		_Interest
	)
	RETURNING savingnum INTO id;

	RETURN id;

END IF;

END;

$$
LANGUAGE 'plpgsql'