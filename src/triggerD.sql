CREATE OR REPLACE FUNCTION triggerD()

RETURNS TRIGGER AS

$$

DECLARE

	customer record;

BEGIN

	DELETE FROM top10customers;

	INSERT INTO top10customers
	SELECT accountnum, balance FROM accountbalance a NATURAL JOIN customers  WHERE
	balance >= 0 AND customers.accountstatus LIKE 'open' AND
	(SELECT count(accountnum) FROM accountbalance NATURAL JOIN customers WHERE customers.accountstatus LIKE 'open' AND balance > a.balance) < 10
	ORDER BY balance DESC;

RETURN new;

END;

$$
LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS triggerD ON accountBalance;

CREATE TRIGGER triggerD AFTER UPDATE OR INSERT OR DELETE ON accountBalance
FOR EACH ROW
EXECUTE PROCEDURE triggerD();