CREATE OR REPLACE FUNCTION triggerA()

RETURNS TRIGGER AS

$$

DECLARE

BEGIN

IF old.balance > 0 THEN

	INSERT INTO actions (
		actionnum,
		accountnum,
		actionname,
		actiondate,
		amount
	)
	VALUES (
		DEFAULT,
		old.accountnum,
		'close',
		CURRENT_DATE,
		(-1) * old.balance
	);

	RETURN old;

ELSIF old.balance < 0 THEN
	RAISE EXCEPTION 'Balance Cannot be negative';
END IF;

RETURN old;

END;

$$
LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS triggerA ON accountBalance;

CREATE TRIGGER triggerA BEFORE DELETE ON accountBalance
FOR EACH ROW
EXECUTE PROCEDURE triggerA();