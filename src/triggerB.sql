CREATE OR REPLACE FUNCTION triggerB()

RETURNS TRIGGER AS

$$

DECLARE

	customer record;

BEGIN


SELECT * INTO customer
FROM customers
WHERE accountnum = old.accountnum;

IF NOT FOUND THEN
	RAISE EXCEPTION 'Customer Unknown';
ELSIF customer.accountstatus = 'close' THEN
	RAISE EXCEPTION 'Account is close';
END IF;

IF new.balance < customer.overdraft THEN
	RAISE EXCEPTION 'Balance Cannot be bellow allowed overdraft';
END IF;

RETURN new;

END;

$$
LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS triggerB ON accountBalance;

CREATE TRIGGER triggerB BEFORE UPDATE ON accountBalance
FOR EACH ROW
EXECUTE PROCEDURE triggerB();