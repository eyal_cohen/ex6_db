select '****** TEST_04 STARTS HERE ****** >>';

\ir triggerA.sql;
\ir triggerB.sql;
-- \ir triggerC.sql;
\ir triggerD.sql;


\ir newCustomer.sql;

select newCustomer (1, 'A', 'p1', 0);
select newCustomer (2, 'B', 'p2', -2000);
select newCustomer (3, 'C', 'p3', -2000);
select newCustomer (4, 'D', 'p4', -2000);
select newCustomer (5, 'D', 'p4', -2000);
--
\ir doAction.sql;

select doAction(1, 'receive', current_date, 10000);
select doAction(2, 'receive', current_date, 10000);
select doAction(3, 'receive', current_date, 10000);
select doAction(4, 'receive', current_date, 10000);
--
\ir newSaving.sql;

select newSaving(1, 400, date '1-1-2012', 1, 0.4);

select newSaving(2, 400, date '1-1-2012', 2, 0.4);

select newSaving(3, 400, date '1-1-2012', 1, 0.4);
select newSaving(3, 400, date '1-1-2012', 1, 0.4);

select newSaving(4, 400, date '1-1-2014', 3, 0.4);
select newSaving(4, 400, date '1-1-2015', 3, 0.4);


select * from accountbalance ORDER BY accountnum;
select * from savings ORDER BY savingnum;
