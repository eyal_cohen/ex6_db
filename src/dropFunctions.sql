DROP FUNCTION IF EXISTS newCustomer
	(
		CustomerID Integer,
		CustomerName VARCHAR,
		CustomerPassword VARCHAR,
		Overdraft Integer
	);

DROP FUNCTION IF EXISTS closeCustomer (
	_CustomerID Integer
	);

DROP FUNCTION IF EXISTS doAction (
	_CustomerID Integer,
	_ActionName Varchar,
	_ActionDate Date,
	_Amount Real
	);

DROP FUNCTION IF EXISTS newSaving (
	_CustomerID Integer,
	_Deposit Integer,
	_DepositDate Date,
	_NumOfYears Integer,
	_Interest Real
	);

DROP FUNCTION IF EXISTS triggerA();
DROP FUNCTION IF EXISTS triggerB();
DROP FUNCTION IF EXISTS triggerC1();
DROP FUNCTION IF EXISTS triggerC2();
DROP FUNCTION IF EXISTS triggerD();