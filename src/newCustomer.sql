CREATE OR REPLACE FUNCTION newCustomer
	(
		_CustomerID Integer,
		_CustomerName VARCHAR,
		_CustomerPassword VARCHAR,
		_Overdraft Integer
	)
RETURNS Integer AS

$$

DECLARE

	id Integer;
	customer record;

BEGIN

SELECT * INTO customer
FROM customers
WHERE customerId = _CustomerID;

IF NOT FOUND THEN
	
	-- account is not exist

	INSERT INTO Customers (
		accountnum,
		customerid,
		customername,
		customerpassword,
		accountstatus,
		overdraft
	)
	VALUES (
		DEFAULT,
		_CustomerID,
		_CustomerName,
		_CustomerPassword,
		'open',
		_Overdraft
	)
	RETURNING accountnum INTO id;

	INSERT INTO AccountBalance (accountnum, balance)
	VALUES (id, 0);

	RETURN id;

ELSIF customer.accountstatus = 'close' THEN

	-- account do exist but close
	
	UPDATE Customers SET 
		customerpassword = _CustomerPassword,
		customername = _CustomerName,
		accountstatus = 'open',
		overdraft = _Overdraft
	WHERE customerid = customer.customerid;

	INSERT INTO AccountBalance (accountnum, balance)
	VALUES (customer.accountnum, 0);

	RETURN customer.accountnum;

END IF;

-- account exists and already open

RETURN -1;

END;

$$
LANGUAGE 'plpgsql'