CREATE OR REPLACE FUNCTION closeCustomer (
	_CustomerID Integer
	)
RETURNS Integer AS

$$

DECLARE

	customer record;

BEGIN

SELECT * INTO customer
FROM customers
WHERE customerid = _CustomerID;

IF NOT FOUND THEN
	RETURN -1;
ELSIF customer.accountstatus = 'close' THEN
	RETURN -1;
ELSE
	DELETE FROM accountBalance
	WHERE accountnum = customer.accountnum;

	DELETE FROM savings
	WHERE accountnum = customer.accountnum;

	UPDATE customers SET accountstatus = 'close'
	WHERE accountnum = customer.accountnum;

	RETURN customer.accountnum;
END IF;

END;

$$
LANGUAGE 'plpgsql'